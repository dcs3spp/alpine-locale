# Overview
Demonstration of using the following solutions for setting locale in alpine linux

- https://github.com/Auswaschbar/alpine-localized-docker.git to set locale en_GB in alpine Linux environment
- Using glibc compatibility libraries to set locale as documented at https://github.com/sgerrand/alpine-pkg-glibc

A debian stretch slim Dockerfile has been provided to demonstrate a working solution
of the python program.

 
## Alpine Usage
```
docker build -t alpine-musl-test -f Dockerfile.musl .
docker build -t alpine-glibc-test -f Dockerfile.glibc .

docker run --rm -i -t alpine-musl-test /bin/ash
python3 test.py

docker run --rm -i -t alpine-glibc-test /bin/ash
python3 test.py
```

## Debian Usage
```
docker build -t debian-test -f Dockerfile.debian .

docker run --rm -i -t debian-test /bin/sh
python3 test.py
```

The python program sets a ISO 6801 UTC date string and prints back a date formatted using
the OS locale. This works from the Debian image but fails for both proposed Alpine solutions.

## Expected behaviour
When the program is run the date should be returned as a GB date string dd/mm/yyyy
The program has been tested and runs on macOS.

## Actual behaviour
When the program is run the date is returned as a US date string mm/dd/yyyy.

